<?php
require_once 'init.php';
if (!isset($_SESSION['nim'])){
  header('Location: login.php');
}
$massage ='';
// delete validation
if ((isset($_GET['delete']) && (isset($_GET['nim']))) && $_SESSION['status'] == 'admin') {
echo "data berhasil di delete";
  if (delete_peserta_on_panitia( $_GET['nim'], $_SESSION['jurusan'])) {
    header('Location:list-peserta.php');

  } else {
    echo "gagal delete";
  }
}
// update validation
if ((isset($_POST['update-data']) && (isset($_POST['nim']))) && $_SESSION['status'] == 'admin') {
  if (update_peserta_on_panitia( $_POST['nim'], $_POST['nama'], $_POST['password'])) {
    echo "data berhasil di update";
  } else {
    echo "gagal update";
  }
}
?>
<?php require_once 'layout/header.php'; ?>
<?php require_once 'layout/menu/m-admin.php' ?>
    <table class="highlight">
      <thead>
        <tr>
          <th data-field="nim">NIM</th>
          <th data-field="nama">NAMA</th>
          <th data-field="password">PASSWORD</th>
          <th data-field="status">STATUS</th>
          <th data-field="action">ACTION</th>
        </tr>
      </thead>
  <?php
      $resault = view_list_peserta($_SESSION['jurusan'], $_SESSION['status']);
      $id = 1;
      $id_delete = 1;
      //looping data dari fungsi view_list_peserta , dan menghasilkan output table
      while ($rows = mysqli_fetch_assoc($resault)) {
  ?>
      <tr>
          <td><?= $rows['nim_mhs' ];  ?></td>
          <td><?= strtoupper($rows['nama_mhs']);  ?></td>
          <td><?= $rows['password'];  ?></td>
          <td><?= strtoupper($rows['status'  ]);  ?></td>
          <td>
            <div class="align-center">
              <a class="modal-trigger" href="#update-data-<?=$id++?>" title="edit data">
                <i class="material-icons al">mode_edit</i>
              </a>
              <a class="modal-trigger" href="#delet-data-<?=$id_delete++?>" title="delet data">
                <i class="small material-icons ">delete</i>
              </a>
            </div>
          </td>
      </tr><?php } //end looping  ?>
  </table>
  <?php
      $resault = view_list_peserta($_SESSION['jurusan'], $_SESSION['status']);
      $id_delete = 1;
      while ($rows = mysqli_fetch_assoc($resault)) {
  ?>
  <div id="delet-data-<?=$id_delete++?>" class="modal">
    <div class="modal-content">

      <h4>Anda Yakin , Ingin Menghapus data ?</h4>
      <p>data <b> <?= $rows['nama_mhs'];  ?> </b> akan di hapus dari database secara permanen</p>
    </div>
    <div class="modal-footer">
      <a class="modal-action modal-close waves-effect waves-green btn-flat" href="list-peserta.php?delete=true&nim=<?= $rows['nim_mhs'];   ?>" title="delet data">Agree</a>
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Disagree</a>
    </div>
  </div>
  <?php } ?>
    <!-- for update data -->
  <?php
      $resault = view_list_peserta($_SESSION['jurusan'], $_SESSION['status']);
      $id = 1;
      while ($rows = mysqli_fetch_assoc($resault)) {
  ?>
  <div id="update-data-<?=$id++?>" class="modal">
    <div class="modal-content">
      <h5>Edit Data :</h5>

      <form class="col s12 m12" method="post" action="list-peserta.php">

        <div class="row ">

          <div class="input-field col s12">
            <input value="<?= $rows['nim_mhs'];   ?>" name="nim" type="text" required>
            <label for="nim">NIM</label>
          </div>

        </div>
        <div class="row">

          <div class="input-field col s12">
            <input value="<?= $rows['nama_mhs'];  ?>" name="nama" type="text" required>
            <label for="nama">NAMA</label>
          </div>

        </div>
        <div class="row">

          <div class="input-field col s12">
            <input value="<?= $rows['password'];  ?>" name="password" type="text" required>
            <label for="password">PASSWORD</label>
          </div>

        </div>
    </div>
    <div class="modal-footer">

      <button class=" modal-action modal-close waves-effect waves-green btn-flat" href="#" >batal</a>
      <button class=" modal-action modal-close waves-effect waves-green btn-flat" type="submit" name="update-data" >simpan</a>

    </div>
    </form>
  </div>
  <?php } ?>
  <script type="text/javascript">$(document).ready(function(){$('.modal-trigger').leanModal();});</script>
<?php require_once 'layout/footer.php'; ?>
