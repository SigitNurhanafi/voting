<?php
require_once 'init.php';

if (isset($_SESSION['nim'])){
  header('Location: index.php');
} else {

$massage='';
if(isset($_POST['login'])){
  $nim      = $_POST['nim'];
  $password = $_POST['password'];

    if (!empty(trim($nim)) && !empty(trim($password))){
      if (checking_user($nim, $password)){

        $resault_query_table = level_user($nim, $password);
        while ($rows = mysqli_fetch_assoc($resault_query_table)) {
            $_SESSION['nim']  =  $rows['nim_mhs'];
            $_SESSION['nama']  =  $rows['nama_mhs'];
            $_SESSION['jurusan']  =  $rows['jurusan'];  
            // $_SESSION['password']  =  $rows['password'];
            $_SESSION['status']  =  $rows['status'];
        }

        header('Location: index.php');
      } else {
        $massage = '<div class="row ">
          <div class="col s12 m4 offset-m4"> NIM atau PASSWORD anda salah</div></div>';
      }
    } else {
      $massage = '<div class="row ">
        <div class="col s12 m4 offset-m4"> harus di isi nim dan password</div></div>';
    }
  }
?>
<?php include_once 'layout/header.php';?>
    </nav>
    <br>
    <div class="container">
      <span class="row center">
        <h5 class="header col s12 light">SELAMAT DATANG DI E-VOTING</h5>
        <h5 class="header col s12 light">PEMILIHAN KETUA DAN WAKIL HIMPUNAN</h5>
        <h5 class="header col s12 light">UNIVERISTAS KOMPUTER INDONESIA</h5>
      </span>
      <?php echo $massage; ?>
      <div class="row ">
        <div class="col s12 m4 offset-m4">
          <div class="card-panel ">
            <div class="row">
              <form class="col s12 m12" action="login.php" method="post" >
                <div class="row ">
                  <div class="input-field col s12">
                    <input name="nim" type="text" required>
                    <label for="nim">NIM</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s12">
                    <input name="password" type="password" required>
                    <label for="password">Password</label>
                  </div>
                </div>
              <button class="btn light-blue lighten-1waves-effect waves-light" type="submit" name="login">LOGIN</button>
              </form>
            </div>
          </div>
        </div>
      </div>
<?php  include_once 'layout/footer.php' ?>
<?php } ?>
