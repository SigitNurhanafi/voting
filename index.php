<?php
require_once 'init.php';
if (!isset($_SESSION['nim'])){
  header('Location: login.php');
} else {
?>
<?php require_once 'layout/header.php'; ?>

<?php
// print_r($_SESSION['nim']);
// print_r($_SESSION['nama']);
// print_r($_SESSION['jurusan']);
// print_r($_SESSION['status']);
if ($_SESSION['status'] == 'admin'){
  include_once 'layout/menu/m-admin.php';
} elseif ($_SESSION['status'] == 'calon') {
  include_once 'layout/menu/m-calon.php';
} elseif ($_SESSION['status'] == 'pemilih') {
  include_once 'layout/menu/m-peserta.php';
}
?>

<?php include_once 'layout/footer.php'; ?>
<?php } ?>
