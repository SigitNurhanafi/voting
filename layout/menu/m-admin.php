<?php

?>
          <li><a href="index.php">MENU ADMIN</a></li>
          <li><a href="logout.php">LOGOUT</a></li>
          <i class="material-icons">settings</i>
        </ul>
      </div>
    </nav>
    <div class="container">

<br>
    <h6 class='left-align'>Selamata Datang, <?=$_SESSION['nama']?> (<?=$_SESSION['nim']?>), <?=$_SESSION['jurusan']?><br><br> Status Anda Sebagai  : <b><?=strtoupper($_SESSION['status'])?></b>
    </h6>
    <br>

    <div class="row">
      <div class="divider"></div>
      <br>
      <div class="col s2 center-align">
        <a href="add-peserta.php">
          <i class="large material-icons">person_add</i>
        </a>
        <br>
      <p class="z-depth-1">Tambah Data Peserta</p>
      </div>

      <div class="col s2 center-align">
      <a href="list-peserta.php" >
        <i class="large material-icons">account_box</i>
      </a>
      <br>
      <p class="z-depth-1">Daftar Data Peserta</p>
      </div>

      <div class="col s2 center-align">
        <a href="add-calon.php">
          <i class="large material-icons">group_add</i>
        </A>
        <br>
        <p class="z-depth-1">Tambah Data Calon</p>
      </div>

      <div class="col s2 center-align">
        <a href="list-calon.php">
          <i class="large material-icons">supervisor_account</i>
        </a>
        <br>
        <p class="z-depth-1">Daftar Data Calon</p>
      </div>

      <div class="col s2 center-align">

        <a href="search.php"><i class="large material-icons">search</i></a>
        <br>
        <p class="z-depth-1">Cari Data Deserta</p>
      </div>

      <div class="col s2 center-align">
        <a href="resault-vote.php">
        <i class="large material-icons">poll</i>
      </a>
        <br>
        <p class="z-depth-1">Hasil Pemilihan Suara</p>

      </div>
    </div>
  <div class="divider"></div>
