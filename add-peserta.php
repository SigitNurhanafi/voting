<?php
require_once 'init.php';

  if(isset($_POST['save'])){
    $nim      = $_POST['nim'];
    $nama     = $_POST['nama'];
    $password = $_POST['password'];

    if (!empty(trim($nim)) && (!empty(trim($nama)) && !empty(trim($password)) ) ) {
        if (register_peserta($nim, $nama, $password, $_SESSION['jurusan'])){
          echo "berhasil";
        } else {
          echo "gagal";
        }
    } else {
      echo "data tidak boleh ada yang dikosongkan";
    }
  }
?>
<?php require_once 'layout/header.php'; ?>
<?php require_once 'layout/menu/m-admin.php' ?>
    <h5>Tambah Data Pemungut Suara :</h5>
    <form class="col s12 m12" method="post" action="add-peserta.php">
      <div class="row ">
        <div class="input-field col s12">
          <input name="nim" type="text" required>
          <label for="nim">NIM</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input name="nama" type="text" required>
          <label for="nama">NAMA</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input name="password" type="text" required>
          <label for="password">PASSWORD</label>
        </div>
      </div>
      <!-- <div class="input-field col s12">
          <select name="jurusan_pst" size= "4">
            <option value=""disabled selected>JURUSAN</option>
            <option value="teknik informatika">TEKNIK INFORMATIKA</option>
            <option value="sistem informasi">SISTEM INFORMASI</option>
            <option value="teknik komputer">TEKNIK KOMPUTER</option>
          </select>
      </div><script>$(document).ready(function(){$('select').material_select();});</script> -->
      <button class="btn light-blue lighten-1waves-effect waves-light" type="submit" name="save">SIMPAN DATA</button>
    </form>
<?php require_once 'layout/footer.php'; ?>
